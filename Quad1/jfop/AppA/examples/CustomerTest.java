package examples;

public class CustomerTest {

    public static void main(String[] args) {
        java.util.Scanner scanner = new java.util.Scanner(System.in);

        Customer custFile[] = new Customer[100];

        int i = 0;
        int j = 0;

        for (i = 0; i < 100; i = i + 1) {
            custFile[i] = inputACustRecord(scanner);
            System.out.print("\nDo you want to enter another "
                    + "customer? (y/n): ");
            String input = scanner.next();
            if (input.charAt(0) == 'n' || input.charAt(0) == 'N')
                break;
        }

        System.out.println("--------- Customers ---------");
        for (j = 0; j <= i; j = j + 1)
            outputACustRecord(custFile[j]);
    }

    public static Customer inputACustRecord(java.util.Scanner scanner) {

        Customer cust = new Customer();

        System.out.print("Please enter the" + " customer's name: ");
        cust.name = scanner.next();
        System.out.print("Please enter the customer's phone"
                + " number: ");
        cust.phone = scanner.next();
        System.out.print("Please enter the customer's customer"
                + " number: ");
        cust.number = scanner.nextInt();
        System.out.print("Please enter the customer's"
                + " credit limit: ");
        cust.creditLimit = scanner.nextFloat();

        return cust;
    }

    public static void outputACustRecord(Customer cust) {
        java.text.DecimalFormat precisionTwo = 
                 new java.text.DecimalFormat("0.00");

        System.out.println("           Name: " + cust.name);
        System.out.println("          Phone: " + cust.phone);
        System.out.println("Customer Number: " + cust.number);
        System.out.println("   Credit Limit: "
                + precisionTwo.format(cust.creditLimit) + "\n");
    }
}
