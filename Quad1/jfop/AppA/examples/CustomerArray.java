package examples;
import java.text.*;

public class CustomerArray {
    public static void main(String[] args) {
        DecimalFormat precisionTwo = new DecimalFormat("0.00");

        Customer cust1 = new Customer();
        Customer custArray[] = new Customer[100];
        cust1.name = "Bill";
        cust1.phone = "555/123-4567";
        cust1.number = 98765;
        cust1.creditLimit = 1000.00F;

        custArray[0] = cust1;
        System.out.println("The stand-alone customer:");
        System.out.println("\tName: " + cust1.name);
        System.out.println("\tPhone: " + cust1.phone);
        System.out.println("\tNumber: " + cust1.number);
        System.out.println("\tLimit: "
                + precisionTwo.format(cust1.creditLimit));

        System.out.println("The customer-array customer:");
        System.out.println("\tName: " + custArray[0].name);
        System.out.println("\tPhone: " + custArray[0].phone);
        System.out.println("\tNumber: " + custArray[0].number);
        System.out.println("\tLimit: "
                + precisionTwo.format(custArray[0].creditLimit));
    }
}
