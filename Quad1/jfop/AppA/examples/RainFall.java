package examples;

public class RainFall {

    public static void main(String[] args) {
        java.util.Scanner scanner = new java.util.Scanner(System.in);

        int i = 0;

        Month[] month = new Month[12]; // Allocate an array of Months

        String monthName[] = { "January  ", "February ", "March    ",
                "April    ", "May      ", "June     ", "July     ",
                "August   ", "September", "October  ", "November ",
                "December " };
        int monthDays[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31,
                30, 31 };

        // Initialize the Month classes
        for (i = 0; i < 12; i++) {
            month[i] = new Month();

            month[i].name = monthName[i];
            month[i].numDays = monthDays[i];
            month[i].avgHigh = 0;
            month[i].avgLow = 0;
            month[i].avgRain = 0;

            System.out.print("Enter the average high for "
                    + month[i].name + ": ");
            month[i].avgHigh = scanner.nextFloat();

            System.out.print("Enter the average low for "
                    + month[i].name + ": ");
            month[i].avgLow = scanner.nextFloat();

            System.out.print("Enter the average rainfall for "
                    + month[i].name + ": ");
            month[i].avgRain = scanner.nextFloat();
        }
        scanner.close();

        System.out.println();

        for (i = 0; i < 12; i++) {
            printMonthStats(month[i]);
        }

        printAnnualStats(month);
    }

    public static void printMonthStats(Month m) {
        System.out.print(m.name + ":\tAvg High: " + m.avgHigh);
        System.out.print("\tAvg Low: " + m.avgLow);
        System.out.println("\tAvg Precip: " + m.avgRain);
    }

    public static void printAnnualStats(Month[] m) {
        float maxtemp, mintemp, maxrain, minrain;
        int hottest = 0, coldest = 0, wettest = 0, driest = 0;
        int k;
        maxtemp = m[0].avgHigh;
        mintemp = m[0].avgLow;
        maxrain = m[0].avgRain;
        minrain = m[0].avgRain;

        for (k = 1; k < 12; k++) {
            if (m[k].avgHigh > maxtemp) {
                maxtemp = m[k].avgHigh;
                hottest = k;
            }

            if (m[k].avgLow < mintemp) {
                mintemp = m[k].avgLow;
                coldest = k;
            }

            if (m[k].avgRain > maxrain) {
                maxrain = m[k].avgRain;
                wettest = k;
            }

            if (m[k].avgRain < minrain) {
                minrain = m[k].avgRain;
                driest = k;
            }
        }

        System.out.println();
        System.out.println();
        System.out.println("Hottest:\n\t" + m[hottest].name + "\t("
                + m[hottest].avgHigh + ")");
        System.out.println("Coldest:\n\t" + m[coldest].name + "\t("
                + m[coldest].avgLow + ")");
        System.out.println("Wettest:\n\t" + m[wettest].name + "\t("
                + m[wettest].avgRain + ")");
        System.out.println("Driest:\n\t" + m[driest].name + "\t("
                + m[driest].avgRain + ")" + "\n");
    }
}
