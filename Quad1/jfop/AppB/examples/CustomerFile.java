package examples;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Scanner;

public class CustomerFile {
    public static void main(String[] args) throws IOException {
        int custCount;

        custCount = inputCustRecords();
        outputCustRecords(custCount);
    }

    public static int inputCustRecords() throws IOException {
        Scanner scanner = new Scanner(System.in);

        int count = 0;
        Customer cust = new Customer();

        FileOutputStream binOut = new FileOutputStream("customer.bin");
        DataOutputStream writeOut = new DataOutputStream(binOut);

        for (count = 0; count < 100; count = count + 1) {
            System.out.print("Please enter the customer's name: ");
            cust.name = scanner.next();
            System.out.print("Please enter the customer's phone number: ");
            cust.phone = scanner.next();
            System.out.print("Please enter the customer's"
                    + " customer number: ");
            cust.number = scanner.nextInt();
            System.out.print("Please enter the customer's"
                    + " credit limit: ");
            cust.creditLimit = scanner.nextFloat();

            writeOut.writeUTF(cust.name);
            writeOut.writeUTF(cust.phone);
            writeOut.writeInt(cust.number);
            writeOut.writeFloat(cust.creditLimit);

            System.out.print("\nDo you want to add another"
                    + " customer? (y/n): ");
            String choice = scanner.next();
            if (choice.charAt(0) == 'n'|| choice.charAt(0) == 'N')
                break;
        }
        scanner.close();
        binOut.close();

        return count;
    }

    public static void outputCustRecords(int custCount)
            throws IOException {

        int count;
        DecimalFormat formatNumber = new DecimalFormat("00000000");
        DecimalFormat formatLimit = new DecimalFormat("0000000000.00");
        Customer cust = new Customer();

        System.out.println("------------------ Customer File"
                + " ------------------");
        System.out.println("Cust. #- Credit Limit- Phone"
                + " ------- Name-----------");

        FileInputStream binIn = new FileInputStream("customer.bin");
        DataInputStream readIn = new DataInputStream(binIn);

        for (count = 0; count <= custCount; count = count + 1) {
            cust.name = readIn.readUTF();
            cust.phone = readIn.readUTF();
            cust.number = readIn.readInt();
            cust.creditLimit = readIn.readFloat();

            System.out.print(formatNumber.format(cust.number) + " ");
            System.out.print(formatLimit.format(cust.creditLimit)
                    + " ");
            System.out.print(cust.phone + " ");
            System.out.println(cust.name);
        }

        binIn.close();
    }
}
