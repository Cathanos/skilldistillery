package examples;

import java.io.*;

public class TextOut {

    public static void main(String[] args) throws IOException {

        String companyOne = "Pam's Pizza";
        String companyTwo = "Bob's Burgers";

        FileWriter txtOut = new FileWriter("food.txt");
        PrintWriter printOut = new PrintWriter(txtOut);

        printOut.println(companyOne);
        printOut.println(companyTwo);

        txtOut.close();
    }
}
