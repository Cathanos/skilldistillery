package examples;

import java.io.*;
import java.text.DecimalFormat;

public class IoText {
  public static void main(String[] args) throws IOException {

    DecimalFormat precisionTwo = new DecimalFormat("0.00");
    Customer cust = new Customer();
    cust.name = "Sidney Q. Flarp";
    cust.phone = "555/123-4567";
    cust.number = 98765;
    cust.creditLimit = 1000.00F;

    FileWriter txtOut = new FileWriter("customer.txt");
    PrintWriter printOut = new PrintWriter(txtOut);

    printOut.println(cust.name);
    printOut.println(cust.phone);
    printOut.println(cust.number);
    printOut.println(precisionTwo.format(cust.creditLimit));
    txtOut.close();
    String inputBuffer = null;
    FileReader conv = new FileReader("customer.txt");
    BufferedReader buf = new BufferedReader(conv);
    inputBuffer = buf.readLine();
    System.out.println("Customer name:\t" + inputBuffer);
    inputBuffer = buf.readLine();
    System.out.println("Customer phone:\t" + inputBuffer);
    inputBuffer = buf.readLine();
    System.out.println("Customer num.:\t" + inputBuffer);
    inputBuffer = buf.readLine();
    System.out.println("Credit limit:\t" + inputBuffer);
    conv.close();
  }
}
