package examples;

import java.io.*;

public class TextErr {

    public static void main(String[] args) throws IOException {

        String companyOne = null;
        String companyTwo = null;

        FileReader txtIn = new FileReader("food.xxx");
        BufferedReader readIn = new BufferedReader(txtIn);

        companyOne = readIn.readLine();
        companyTwo = readIn.readLine();
        txtIn.close();

        System.out.println(companyOne);
        System.out.println(companyTwo);
    }
}
