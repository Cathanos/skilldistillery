package examples;

import java.io.*;

public class TextIn {

    public static void main(String[] args) throws IOException {

        String companyOne = null;
        String companyTwo = null;

        FileReader txtIn = new FileReader("food.txt");
        BufferedReader readIn = new BufferedReader(txtIn);

        companyOne = readIn.readLine();
        companyTwo = readIn.readLine();
        txtIn.close();

        System.out.println(companyOne);
        System.out.println(companyTwo);
    }
}
